module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        mytheme: {
          lighter: "#FCF8FF",
          light: "#BC8DF4",
          DEFAULT: "#8D61C4",
          dark: "#603996",
          darker: "#34116B"
        }
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  mode: 'jit'
}
